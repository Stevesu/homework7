
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   class TreeNode {

      /**
       * Getter of the TreeNode name, for leaves also representing the unique character from the input.
       * @return name as String.
        */
      public String getName() {
         return name;
      }

      /**
       * Setter of the TreeNode name, for leaves also representing the unique character from the input.
       * @param name as String.
        */
      public void setName(String name) {
         this.name = name;
      }

      /**
       * Getter of the Byte representation of a character from a TreeNode.
       * @return code as Byte.
        */
      public Byte getCode() {
         return code;
      }

      /**
       * Setter for the Byte representation of a character to a TreeNode.
       * @param code as Byte.
        */
      public void setCode(Byte code) {
         this.code = code;
      }

      /**
       * Getter for the occurance from a TreeNode.
       * @return occurance as Integer.
        */
      public Integer getOccurance() {
         return occurance;
      }

      /**
       * Setter for the occurance to a TreeNode.
       * @param occurance as Integer.
        */
      public void setOccurance(Integer occurance) {
         this.occurance = occurance;
      }

      /**
       * Getter for the first child of a TreeNode.
       * @return firstChild as TreeNode.
       */
      public TreeNode getFirstChild() {
         return firstChild;
      }

      /**
       * Setter for the first child of a TreeNode.
       * @param firstChild as TreeNode.
       */
      public void setFirstChild(TreeNode firstChild) {
         this.firstChild = firstChild;
      }

      /**
       * Getter of the first child bit representation of a TreeNode.
       * @return firstChildBit as String.
       */
      public String getFirstChildBit() {
         return firstChildBit;
      }

      /**
       * Setter for the second first bit representation of a TreeNode.
       * @param firstChildBit as a String.
       */
      public void setFirstChildBit(String firstChildBit) {
         this.firstChildBit = firstChildBit;
      }

      /**
       * Getter for the second child of a TreeNode.
       * @return secondChild as TreeNode.
        */
      public TreeNode getSecondChild() {
         return secondChild;
      }

      /**
       * Setter for the second child of a TreeNode.
       * @param secondChild as TreeNode.
        */
      public void setSecondChild(TreeNode secondChild) {
         this.secondChild = secondChild;
      }

      /**
       * Getter of the second child bit representation of a TreeNode.
       * @return secondChildBit as String.
        */
      public String getSecondChildBit() {
         return secondChildBit;
      }

      /**
       * Setter for the second child bit representation of a TreeNode.
       * @param secondChildBit as a String.
        */
      public void setSecondChildBit(String secondChildBit) {
         this.secondChildBit = secondChildBit;
      }

      private String name;
      private Byte code;
      private Integer occurance;
      private TreeNode firstChild;
      private String firstChildBit;
      private TreeNode secondChild;
      private String secondChildBit;
      private String finalBit;

      /**
       * Constructor of the TreeNode
       * @param c as String. Name of the TreeNode, character the TreeNode is representing.
       * @param x as Byte. Byte representation of the TreeNode. Only used for leaves.
       * @param n as Integer. Number of occurance of a current character in a byte[].
       * @param t as TreeNode. First child of the current TreeNode. Null for leaves.
       * @param a as String. Assigned bit to the first child of the current TreeNode. Null for leaves.
       * @param p as TreeNode. Second child of the current TreeNode. Null for leaves.
       * @param b as String. Assigned bit the the second child of the current TreeNode. Null for leaves.
       * @param f as String. Final bit representation of the current TreeNode. Only used for leaves.
       */
      TreeNode(String c, Byte x, Integer n, TreeNode t, String a, TreeNode p, String b, String f) {
         this.setName(c);
         this.setCode(x);
         this.setOccurance(n);
         this.setFirstChild(t);
         this.setFirstChildBit(a);
         this.setSecondChild(p);
         this.setSecondChildBit(b);
         this.setFinalBit(f);
      }

      /**
       * Check if TreeNode has children.
       * @return boolean, true if TreeNode has children.
        */
      public boolean hasChildren() {
         return (getFirstChild() != null || getSecondChild() != null);
      }

       /**
        * Getter for the final bit for a TreeNode.
        * @return finalBit as String.
         */
      public String getFinalBit() {
         return finalBit;
      }

      /**
       * Setter for the final bit for a TreeNode.
       * @param finalBit as String representation of a bit.
        */
      public void setFinalBit(String finalBit) {
         this.finalBit = finalBit;
      }
   }

   private byte[] byteArray;
   private ArrayList<TreeNode> leaves;
   private int length;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      if (original.length == 0 || original.equals(null)) {
         throw new RuntimeException("Sisend ei saa olla tühi!");
      }
      byteArray = original;
      leaves = new ArrayList<TreeNode>();
   }

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {
      return length;
   }

   /** Encoding the byte array using this prefixcode.
    * @param origData original data.
    * @return encoded data.
    */
   public byte[] encode (byte [] origData) {
         // Find input length:
         int byteArrayLength = origData.length;
         System.out.println("Length of original data in bytes: " + byteArrayLength);

         //List the unique characters:
         List<Byte> uniqueCharacters = uniqueCharacters(origData);

         // Count unique characters:
         int nUnique = uniqueCharacters.size();

         // Create a TreeNode array:
         TreeNode[] tree = new TreeNode[nUnique];

         // Find the leaves of the tree and add them to tree array:
         for (int i = 0; i < nUnique; i++) {
            byte nodeCode = uniqueCharacters.get(i);
            String nodeName = Character.toString((char) nodeCode);
            int nodeOccurance = countOccurance(origData, nodeName);
            tree[i] = new TreeNode(nodeName, nodeCode, nodeOccurance, null, null, null, null, null);
         }

         TreeNode[] treeTemp = tree;
         // While there are at least 2 nodes in the Array:
         while (treeTemp.length > 1) {
            //  Find 2 minimum nodes in the array and remove them from the array:
            TreeNode firstMinNode = findMinNode(treeTemp);
            treeTemp = removeNodeFromArray(treeTemp, firstMinNode);
            TreeNode secondMinNode = findMinNode(treeTemp);
            treeTemp = removeNodeFromArray(treeTemp, secondMinNode);

            //  Create a parentNode for the 2 minimum nodes and add it to the array
            String parentName = firstMinNode.getName() + secondMinNode.getName();
            int parentOccurence = firstMinNode.getOccurance() + secondMinNode.getOccurance();
            TreeNode parent = new TreeNode(parentName, null, parentOccurence, firstMinNode, "0", secondMinNode, "1", null);
            treeTemp = addNodeToArray(treeTemp, parent);
         }

         //  Create an array with final leaves:
         TreeNode topNode = treeTemp[0];
         String code = new String();
         if (nUnique == 1) code = "0";
         TreeNode[] finalLeaves = new TreeNode[nUnique];
         findLeaves(topNode, code, finalLeaves);

         // Encode the input:
         StringBuilder sb = new StringBuilder();
         int bitSize = 0;
         for (int i = 0; i < byteArrayLength; i++) {
            for (int j = 0; j < nUnique; j++) {
               if (origData[i] == finalLeaves[j].getCode()) {
                  sb.append(finalLeaves[j].getFinalBit());
                  bitSize = bitSize + finalLeaves[j].getFinalBit().length();
               }
            }
         }
         length = sb.length();
         int bitsInBytes = bitsToBytes(sb.length());
         System.out.println("Length of encoded data in bits: " + bitSize + " (" + bitsInBytes + " bytes)");
         System.out.println("Encoded input: " + sb);
         String encodedInput = sb.toString();

         byte[] returnEncoded = new byte[bitsInBytes];
         int startIndex = 0;
         int endIndex = 8;
         String remaining = encodedInput.substring(startIndex, encodedInput.length());
         for (int i = 0; i < bitsInBytes; i++) {
            if (endIndex < encodedInput.length()) {
               // System.out.println(encodedInput.substring(startIndex, endIndex));
               returnEncoded[i] = (byte) Integer.parseInt(encodedInput.substring(startIndex, endIndex), 2);
               startIndex = startIndex + 8;
               endIndex = endIndex + 8;
               remaining = encodedInput.substring(startIndex, encodedInput.length());
            } else if (remaining.length() < 8 && remaining.length() > 0) {
               StringBuilder finalByte = new StringBuilder();
               for (int j = 0; j < 8; j++) {
                  if (remaining.length() > j) {
                     finalByte.append(remaining.charAt(j));
                  } else {
                     finalByte.append(0);
                  }
               }
               // System.out.println(finalByte.toString());
               returnEncoded[i] = (byte) Integer.parseInt(finalByte.toString(), 2);
            }
         }
         return returnEncoded;
      }

   /**
    * Converts bits to Bytes.
    * @param bits as int. Number of bits.
    * @return nBytes as number of bytes.
     */
   private int bitsToBytes(int bits) {
      int nBytes;
      if (bits%8!=0){
         nBytes = bits/8+1;
      } else {
         nBytes = bits/8;
      }
      return nBytes;
   }

   /**
    * Finds all leaves of a TreeNode;
    * @param node as TreeNode that leaves are sought.
    * @param code as String. Preparation for a bit representation of a leaf.
    * @param finalLeaves as TreeNode[] of leaves of the input TreeNode.
     */
   private void findLeaves(TreeNode node, String code, TreeNode[] finalLeaves) {
      if(node.hasChildren()){
         findLeaves(node.getFirstChild(), code+node.getFirstChildBit(), finalLeaves);
         findLeaves(node.getSecondChild(), code+node.getSecondChildBit(), finalLeaves);
      } else {
         node.setFinalBit(code);
         int pointer = 0;
         for (int i = 0; i < finalLeaves.length; i++) {
            if (finalLeaves[i] == null) {
               pointer = i;
               break;
            }
         }
         System.out.println("Symbol " + node.getName() + " code: " + node.getCode() + " frequency  " + node.getOccurance()  + " and hcodeLength " + node.getFinalBit().length() + " bits hcode " + node.getFinalBit());
         finalLeaves[pointer] = node;
         leaves.add(node);
      }
   }

   /**
    * Adds a TreeNode to a TreeNode[].
    * @param tree as TreeNode[] to which a TreeNode will be added.
    * @param node as TreeNode which will be added to the TreeNode[]
    * @return newTree as a TreeNode[].
    */
   private TreeNode[] addNodeToArray(TreeNode[] tree, TreeNode node) {
      TreeNode[] newTree = new TreeNode[tree.length+1];
      for (int i = 0; i < tree.length; i++) {
            newTree[i] = tree[i];
         }
      newTree[tree.length]=node;
      return newTree;
   }

   /**
    * Removes a TreeNode from a TreeNode[].
    * @param tree as TreeNode[] from which a TreeNode will be removed.
    * @param node as TreeNode which will be removed from the TreeNode[]
    * @return newTree as a TreeNode[].
     */
   private TreeNode[] removeNodeFromArray(TreeNode[] tree, TreeNode node) {
      TreeNode[] newTree = new TreeNode[tree.length-1];
      int counter = 0;
      for (int i = 0; i < tree.length; i++) {
         if (tree[i] != node) {
            newTree[counter] = tree[i];
            counter++;
         }
      }
      return newTree;
   }

   /**
    * Finds the TreeNode with least occurances in a TreeNode[].
    * @param tree as a TreeNode[] from which the node with least occurances is sought.
    * @return minNode as a TreeNode with least occurances.
     */
   private TreeNode findMinNode(TreeNode[] tree) {
      TreeNode minNode = tree[0];
      for (int i = 1; i < tree.length; i++) {
         if (tree[i].getOccurance() < minNode.getOccurance()) minNode = tree[i];
      }
      return minNode;
   }

   /**
    * Lists all the unique characters in a given byte[];
    * @param origData as byte[] which unique characters are listed.
    * @return list as a List of unique characters as bytes.
     */
   private List<Byte> uniqueCharacters(byte[] origData) {
      List<Byte> list = new LinkedList<Byte>();
      byte[] temp = origData;

      while (temp.length>0) {
         byte first = temp[0];
         String firstChar = Character.toString((char) first);
         int nOccurance = countOccurance(temp, firstChar); // Sümboli esinemise arv
         list.add(first);
         temp = findRemainingByteArray(temp, nOccurance, first);;
      }
      return list;
   }

   /**
    * Creates a new byte[] by removing all occurances of a given byte from the input byte[].
    * @param tempCopy as byte[].
    * @param nOccurance as int. Number of times byte "first" occurs in byte[].
    * @param first as byte to be removed from the byte[].
    * @return remaining as byte[].
     */
   private byte[] findRemainingByteArray(byte[] tempCopy, int nOccurance, byte first) {
      byte[] remaining = new byte[tempCopy.length-nOccurance];
      int countDifferent = 0;
      for (int j = 0; j < tempCopy.length; j++) {
         if (tempCopy[j] != first) {
            remaining[countDifferent] = tempCopy[j];
            countDifferent++;
         }
      }
      return remaining;
   }

   /**
    * Counts the occurance of a character in a byte[].
    * @param tempCopy as byte[].
    * @param s as String that is expected to be a character.
    * @return nOccurance as int. Number of occurances of s in tempCopy.
     */
   private int countOccurance(byte[] tempCopy, String s) {
      int nOccurance = 0;
      char c = s.charAt(0);
      for (int j = 0; j < tempCopy.length; j++) {
         if (tempCopy[j] == c) nOccurance++;
      }
      return nOccurance;
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
     */
   public byte[] decode (byte[] encodedData) {
      String decodeInput = byteToString(encodedData);

      StringBuilder decodedString = new StringBuilder();

      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < length; i++) {
         sb.append(decodeInput.charAt(i));
         for (int j = 0; j < leaves.size(); j++) {
            if (sb.toString().equals(leaves.get(j).getFinalBit())){
               decodedString.append(leaves.get(j).getName());
               sb = new StringBuilder();
            }
         }
      }
      System.out.println("Decoded string: " + decodedString);
      System.out.println();

      return decodedString.toString().getBytes();
   }

   /**
    * Converts a byte array to a binary string.
    * @param encodedData as byte[]. Input byte array.
    * @return sb.toString as String. Binary representation of the input array.
     */
   private String byteToString(byte[] encodedData) {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < encodedData.length; i++) {
         sb.append(String.format("%8s", Integer.toBinaryString(encodedData[i] & 0xFF)).replace(' ', '0'));
      }
      StringBuilder correctLength = new StringBuilder();
      for (int i = 0; i < length; i++) {
         correctLength.append(sb.toString().charAt(i));
      }
      // System.out.println("Decoded input: " + correctLength.toString());
      return sb.toString();
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      System.out.println("Input text: " + tekst);
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }
}